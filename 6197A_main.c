#pragma config(Sensor, in1,    SecondBattery,  sensorAnalog)
#pragma config(Sensor, dgtl1,  LEncdr,         sensorQuadEncoder)
#pragma config(Sensor, dgtl3,  REncdr,         sensorQuadEncoder)
#pragma config(Sensor, dgtl5,  RLiftEncoder,   sensorQuadEncoder)
#pragma config(Sensor, dgtl7,  LLiftEncoder,   sensorQuadEncoder)
#pragma config(Sensor, dgtl9,  Trigger,        sensorTouch)
#pragma config(Sensor, dgtl10, Repeat,         sensorTouch)
#pragma config(Motor,  port1,           RightLift1,    tmotorVex393, openLoop, reversed)
#pragma config(Motor,  port2,           LFDrv,         tmotorVex393, openLoop, reversed)
#pragma config(Motor,  port3,       RFDrv,         tmotorVex393, openLoop)
#pragma config(Motor,  port4,           RBDrv,         tmotorVex393, openLoop)
#pragma config(Motor,  port5,           LBDrv,         tmotorVex393, openLoop, reversed)
#pragma config(Motor,  port6,           LeftLift2,     tmotorVex393, openLoop)
#pragma config(Motor,  port7,           RightLift2,    tmotorVex393, openLoop, reversed)
#pragma config(Motor,  port8,           IntakeL,       tmotorVex393, openLoop)
#pragma config(Motor,  port9,           IntakeR,       tmotorVex393, openLoop)
#pragma config(Motor,  port10,          LeftLift1,     tmotorVex393, openLoop)

#pragma platform(VEX2)

#pragma competitionControl(Competition)
#pragma autonomousDuration(2000)
#pragma userControlDuration(120)

#include "Elite_Includes.c" // Must be in quotations!
#include "Manipulator_Functions.c"
#include "Drive_Functions.c"
#include "Autonomous_Independent.c"

void pre_auton()
{
	lift_goal = 0;
	StartTask(GetAutonomous);
	bStopTasksBetweenModes = true;
}

task autonomous()
{
	PlaySoundFile("WindowsShutdown.wav");
	StartTask(AutonomousIndependent);
}

task usercontrol()
{
	StopTask(GetAutonomous);
	lift_goal = last_arm_pos;
	StartTask(arm);
	while(true)
	{
		if(lift_goal >= 200)
		{
			LDrv = vexRT[Ch3] * 0.55;
			RDrv = vexRT[Ch2] * 0.55;
		}
		else
		{
			LDrv = vexRT[Ch3];
			RDrv = vexRT[Ch2];
		}

		if(vexRT[Btn5U] == 1)
		{
			Intake = 127;
		}
		else if(vexRT[Btn5D] == 1)
		{
			Intake = -127;
		}
		else
		{
			Intake = 0;
		}
	}
}
